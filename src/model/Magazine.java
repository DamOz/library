package model;

import java.time.MonthDay;
import java.util.Objects;

public class Magazine extends Publication{
    public static final String TYPE = "Magazyn";

    private MonthDay monthDay;
    private String language;

    @Override
    public String toCsv() {
        return (TYPE + ";") +
                getTitle() + ";" +
                getPublisher() + ";" +
                getYear() + ";" +
                getMonthDay() + ";" +
                language + "";
    }

    public Magazine(int year, String title, String publisher, MonthDay monthDay, String language) {
        super(year, title, publisher);
        this.monthDay = monthDay;
        this.language = language;
    }

    public MonthDay getMonthDay() {
        return monthDay;
    }

    public void setMonthDay(MonthDay month) {
        this.monthDay = monthDay;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return super.toString() +
                "monthDay=" + monthDay +
                ", language='" + language + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return monthDay == magazine.monthDay &&
                Objects.equals(language, magazine.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), monthDay, language);
    }
}
