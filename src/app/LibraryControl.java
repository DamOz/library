package app;

import exception.DataExportException;
import exception.DataImportException;
import exception.NoSuchOptionException;
import exception.UserAlreadyExistsException;
import file.FileManager;
import file.FileManagerBuilder;
import io.ConsolePrinter;
import io.DataReader;
import model.*;
import java.util.InputMismatchException;


public class LibraryControl {

    private ConsolePrinter printer = new ConsolePrinter();
    private DataReader dataReader = new DataReader(printer);
    private FileManager fileManager;


    private Library library = new Library();

    LibraryControl(){
        fileManager = new FileManagerBuilder(printer, dataReader).build();
        try{
            library = fileManager.importData();
            printer.printLine("Zaimportowane dane z pliku");
        } catch (DataImportException e){
            printer.printLine(e.getMessage());
            printer.printLine("Zainicowano nową bazę.");
            library = new Library();
        }
    }

    public void controlLoop() {
        Option option;

        do {
            printOptions();
            option = getOption();
            switch (option) {
                case ADD_BOOK:
                    addBook();
                    break;
                case ADD_MAGAZINE:
                    addMagazine();
                    break;
                case PRINT_BOOKS:
                    printBooks();
                    break;
                case PRINT_MAGAZINES:
                    printMagazine();
                    break;
                case DELETE_BOOK:
                    deleteBook();
                    break;
                case DELETE_MAGAZINE:
                    deleteMagazin();
                    break;
                case ADD_USER:
                    addUser();
                    break;
                case PRINT_USERS:
                    printUsers();
                    break;
                case FIND_BOOK:
                    findBook();
                    break;
                case EXIT:
                    exit();
                    break;
                default:
                    System.out.println("Nie ma takiej opcji wybierz ponowanie.");

            }
        }
        while(option != Option.EXIT);
        }
private Option getOption(){
        boolean optionOk = false;
        Option option = null;
        while (!optionOk){
            try{
                option = Option.createFromInt(dataReader.getInt());
                optionOk = true;
            } catch (NoSuchOptionException e){
                printer.printLine(e.getMessage() + " , podaj ponowanie: ");
            } catch (InputMismatchException e){
                printer.printLine("Wprowadzono wartość, która nie jest liczbą, podaj liczbę: ");
            }
        }
        return option;
}
    private void addUser() {
        LibraryUser libraryUser = dataReader.createLibraryUser();
        try {
            library.addUser(libraryUser);
        } catch (UserAlreadyExistsException e) {
            printer.printLine(e.getMessage());
        }
    }

    private void addBook(){
        try {
            Book book = dataReader.readAndCreateBook();
            library.addPublication(book);
        }catch(InputMismatchException e) {
            printer.printLine("Nie udało się utworzyć książki, niepoprawne dane.");
        }catch (ArrayIndexOutOfBoundsException e) {
            printer.printLine("Osiągnięto limit pojemności, nie można dodać kolejnej książki.");
        }
    }
    private void addMagazine() {
        try {
            Magazine magazine = dataReader.readAndCreateMagazine();
            library.addPublication(magazine);
        } catch (InputMismatchException e) {
            printer.printLine("Nie udało się utworzyć magazynu, niepoprawne dane.");
        } catch (ArrayIndexOutOfBoundsException e) {
            printer.printLine("Osiągnięto limit pojemności, nie można dodać kolejnego magazynu.");
        }
    }

    private void deleteBook(){
        try{
            Book book = dataReader.readAndCreateBook();
            if(library.removePublication(book)){
                printer.printLine("Usunięto ksiązkę");
            } else
                printer.printLine("Brak wprowadzej książki");
        } catch (InputMismatchException e) {
            printer.printLine("Niepoprwanie wprowadzona książka.");
        }
    }
    private void deleteMagazin(){
        try{
            Magazine magazine = dataReader.readAndCreateMagazine();
            if(library.removePublication(magazine)){
                printer.printLine("Usunięto magazyn");
            } else
                printer.printLine("Brak wprowadzego magazynu");
        } catch (InputMismatchException e) {
            printer.printLine("Niepoprwanie wprowadzony magazyn.");
        }
    }

    private void printOptions() {
        printer.printLine("Wybierz opcję: ");
       for(Option option:Option.values()){
           printer.printLine(option.toString());
       }
    }
    private void printBooks(){
           printer.printBooks(library.getSortedPublcations((l1, l2) -> l1.getTitle().compareToIgnoreCase(l2.getTitle())));
    }
    private void printMagazine(){
        printer.printMagazines(library.getSortedPublcations((l1, l2) -> l1.getTitle().compareToIgnoreCase(l2.getTitle())));
    }
    private void printUsers(){
        printer.printUsers(library.getSortedUsers((l1, l2) -> l1.getLastName().compareToIgnoreCase(l2.getLastName())));
    }

    private void findBook(){
        printer.printLine("Podaj tytuł publikacji:");
        String title = dataReader.getString();
        String notFoundMessage = "Brak wyszukiwanej publikacji";
        library.findPublicationByTitle(title)
                .map(Publication::toString)
                .ifPresentOrElse(System.out::println, () -> System.out.println(notFoundMessage));
    }

    private void exit(){
        try{
            fileManager.exportData(library);
            printer.printLine("Export danych do pliku zakończony powodzeniem");
        } catch (DataExportException e) {
            printer.printLine(e.getMessage());
        }
        printer.printLine("Koniec programu.");
        dataReader.close();

    }
    private enum Option {
        EXIT(0, "Wyjście z programu."),
        ADD_BOOK(1, "Dodaj książkę."),
        ADD_MAGAZINE(2, "Dodaj magazyn"),
        PRINT_BOOKS(3, "Wydrukuj książki."),
        PRINT_MAGAZINES(4, "Wydrukuj magazyny"),
        DELETE_BOOK(5, "Usuń książkę"),
        DELETE_MAGAZINE(6, "Usuń magazyn"),
        ADD_USER(7, "Dodaj czytelnika"),
        PRINT_USERS(8, "Wyświetl czytelników"),
        FIND_BOOK(9,"Znajdź książkę");

        private int value;
        private String description;


        Option(int value, String description) {
            this.value = value;
            this.description = description;
        }

        public String toString(){
            return value + " - " + description;
        }

        static Option createFromInt(int option) throws NoSuchOptionException{
            try {
                return Option.values()[option];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new NoSuchOptionException("Brak takiej opcji o id " + option);
            }
        }


    }
}