package exception;

public class NoSuchFileException extends RuntimeException{
    public NoSuchFileException(String massage) {
        super(massage);
    }
}
