package io;


import model.Book;
import model.LibraryUser;
import model.Magazine;

import java.util.Scanner;

public class DataReader {
    private Scanner sc = new Scanner(System.in);
    private ConsolePrinter printer;

    public DataReader(ConsolePrinter printer){
        this.printer = printer;
    }
    public String getString() {
        return sc.nextLine();
    }
    public LibraryUser createLibraryUser() {
        printer.printLine("Imię");
        String firstName = sc.nextLine();
        printer.printLine("Nazwisko");
        String lastName = sc.nextLine();
        printer.printLine("Pesel");
        String pesel = sc.nextLine();
        return new LibraryUser(firstName, lastName, pesel);
    }
    public LibraryUser createUserFromString(String csvText) {
        String[] split = csvText.split(";");
        String firstName = split[0];
        String lastName = split[1];
        String pesel = split[2];

        return new LibraryUser(firstName, lastName, pesel);
    }

    public void close() {
        sc.close();
    }
    public int getInt() {
        try{
            return sc.nextInt();
        } finally {
            sc.nextLine();
        }
    }

    public Book readAndCreateBook() {
        printer.printLine("Tytuł: ");
        String title = sc.nextLine();
        printer.printLine("Autor: ");
        String author = sc.nextLine();
        printer.printLine("Wydawnictwo: ");
        String publisher = sc.nextLine();
        printer.printLine("ISBN: ");
        String isbn = sc.nextLine();
        printer.printLine("Rok wydania: ");
        int year = sc.nextInt();
        sc.nextLine();
        printer.printLine("Ilość stron: ");
        int pages = sc.nextInt();
        sc.nextLine();

        return new Book(year, title, publisher, author, pages, isbn);
    }

    public Magazine readAndCreateMagazine(){
        printer.printLine("Tytuł: ");
        String title = sc.nextLine();
        printer.printLine("Autor: ");
        int month = sc.nextInt();
        printer.printLine("Wydawnictwo: ");
        String publisher = sc.nextLine();
        printer.printLine("ISBN: ");
        int day = sc.nextInt();
        printer.printLine("Rok wydania: ");
        int year = sc.nextInt();
        sc.nextLine();
        printer.printLine("Ilość stron: ");
        String language = sc.nextLine();
        sc.nextLine();

        return new Magazine(year, title, publisher, month, day, language );
    }


}
