package app;

import exception.NoSuchOptionException;

public enum Option {
    EXIT(0, "Wyjście z programu."),
    ADD_BOOK(1, "Dodaj książkę."),
    ADD_MAGAZINE(2, "Dodaj magazyn"),
    PRINT_BOOKS(3, "Wydrukuj książki."),
    PRINT_MAGAZINES(4, "Wydrukuj magazyny");

    private int value;
    private String description;


    Option(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public String toString(){
        return value + " - " + description;
    }

    static Option createFromInt(int option) throws NoSuchOptionException{
        try {
            return Option.values()[option];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NoSuchOptionException("Brak takiej opcji o id " + option);
        }
    }
}
